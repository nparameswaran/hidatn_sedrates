#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 26 12:52:52 2023

@author: sunms498
"""

#/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 14 12:21:43 2022

@author: paramnav
"""

import torch
from pathlib import Path
import pickle
import numpy as np
from matplotlib import pyplot as plt
#from sklearn.model_selection import StratifiedKFold, KFold
#from sklearn.model_selection import train_test_split
import os


os.chdir("/gxfs_work1/cau/sunms498/work/david/NN4SedRatePred/models/MLP/SedRate_MAR/models_torch/")


np.random.seed(42)
torch.manual_seed(42)
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
#print(physical_devices)

print(device)

from os import walk  
feature_path = Path(Path() / "../../../../data/processed/trainingdata/SedFeatures_noNAN")
X_mean = np.load(Path(feature_path / "features_mean.npy")).astype(np.float)
X_std = np.load(Path(feature_path / "features_std.npy")).astype(np.float)

#dataset_path = Path(Path().resolve().parents[2] / "Data" / "LeeKNNsTOC" / "WorldFeaturesAll")#ChlorWorldFeatures
dataset_path = Path("../../../../../../../Data/RestrepoKNNsSed/WorldFeaturesAllnoNaN_AE")
files = []

for (dirpath, dirnames, filenames) in walk(dataset_path):
    files.extend(filenames)
    break

files.sort()


features = np.load(Path(dataset_path / files[0]))
"""
# import dask.array as da
for count, file in enumerate(files):
    


    features = np.load(Path(dataset_path / file))
    # features = features[:,selection_filter]
    #features = features[:,~nan_filter] #filter NaNs

    #Norm Features
    features = np.divide((features - X_mean),X_std)
    
    #scale down the features by 10
    features = features/10

    features = torch.tensor(features)

    batch_size = 4096 # USe batch size in the degree of 2, since it is better to train in the gpu 

    dataset=TensorDataset(features, features)
    #dataloader=DataLoader(dataset, batch_size=batch_size)



    testsize=int(features.shape[0]/10)
    trainsize=features.shape[0]-testsize


    train_set, test_set = torch.utils.data.random_split(dataset, [trainsize, testsize], generator=torch.Generator().manual_seed(42))

    trainloader= torch.utils.data.DataLoader(train_set, batch_size=batch_size, shuffle=True)
    testloader= torch.utils.data.DataLoader(test_set, batch_size=batch_size, shuffle=True)
    torch.save(trainloader, "../../data/processed/trainloaders/trainloader_"+str(count)+".pth")
    torch.save(testloader, "../../data/processed/testloaders/testloader_"+str(count)+".pth")
    
    print("DataLoader for patch "+ str(count)+ "done")



"""    


#features = np.load(Path(dataset_path / files[0]))
#PCA
# pca = PCA (0.95) #95% of the variance is retained
# pca.fit(features)
# features = pca.transform(features)

#print(features.shape)
#feature_path = Path(Path() / "SedFeatures") #ChlorFeatures
#with open(Path(feature_path / "sorted_features.txt"), "rb") as fp:
#    feature_names = pickle.load(fp)

print("feature shape")

print(features.shape)
        

class AE(torch.nn.Module):
    def __init__(self,  layer_width):
        super().__init__()
         
        # Building an linear encoder with Linear
        # layer followed by Relu activation function
        # 784 ==> 9
        self.do_prob = 0.2
        self.encoder = torch.nn.Sequential(
            torch.nn.Linear(features.shape[1], layer_width[0]),
            torch.nn.BatchNorm1d(layer_width[0]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[0], layer_width[1]),
            torch.nn.BatchNorm1d(layer_width[1]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[1], layer_width[2]),
            torch.nn.BatchNorm1d(layer_width[2]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[2], layer_width[3]),
            torch.nn.BatchNorm1d(layer_width[3]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[3], layer_width[4]),
            torch.nn.BatchNorm1d(layer_width[4]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[4], layer_width[5]),
            torch.nn.BatchNorm1d(layer_width[5]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[5], layer_width[6]),
            torch.nn.BatchNorm1d(layer_width[6]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[6], layer_width[7]),
            torch.nn.BatchNorm1d(layer_width[7]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[7], layer_width[8]),
            torch.nn.BatchNorm1d(layer_width[8]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[8], layer_width[9])
        )
         
        # Building an linear decoder with Linear
        # layer followed by Relu activation function
        # The Sigmoid activation function
        # outputs the value between 0 and 1
        # 9 ==> 784
        self.decoder = torch.nn.Sequential(
            torch.nn.Linear(layer_width[9], layer_width[8]),
            torch.nn.BatchNorm1d(layer_width[8]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[8], layer_width[7]),
            torch.nn.BatchNorm1d(layer_width[7]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[7], layer_width[6]),
            torch.nn.BatchNorm1d(layer_width[6]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[6], layer_width[5]),
            torch.nn.BatchNorm1d(layer_width[5]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[5], layer_width[4]),
            torch.nn.BatchNorm1d(layer_width[4]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[4], layer_width[3]),
            torch.nn.BatchNorm1d(layer_width[3]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[3], layer_width[2]),
            torch.nn.BatchNorm1d(layer_width[2]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[2], layer_width[1]),
            torch.nn.BatchNorm1d(layer_width[1]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[1], layer_width[0]),
            torch.nn.BatchNorm1d(layer_width[0]),
            torch.nn.ReLU(),
            torch.nn.Dropout(self.do_prob),
            torch.nn.Linear(layer_width[0], features.shape[1])
        )
 
    def forward(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded
    
training_accuracy = []
training_losses = []
training_mae = []
learning_rate =  []

def train(epoch, trainloader, change_lr):
    model.train()   #tells the model that you are training the model. layers such as dropout and batchnorm which might 
                    #have a different characteristic during training and testing know what is going on. 
    running_loss = 0
    # total = 0
    # correct = 0
    counter = 0
    mae_epoch = 0
    running_vall_loss = 0.0 
    # running_accuracy = 0.0 
    # total = 0 
    
    for features, labels in trainloader:
        
#         if type(features) == list:
#             print(features[0])
        #print(features.shape)
        if torch.cuda.is_available():
            #print(features)
            features = features.cuda()
            #labels = labels.cuda()

        features_detached = features.detach()
        
        #print(features[0].shape)
        features = features.to(dtype = torch.double)
        #print(features.shape)
        #labels   = labels.to(dtype = torch.double) 
        #summary(model, (num_channels, 7,7))
        #Training pass
        optimizer.zero_grad()
        

        
        output = model(features_detached) #predictions
        #labels = torch.reshape(labels, (labels.shape[0],1))
        #output = torch.reshape(output, (output.shape[0],1))
        
        #print(output.shape)
        #print(labels.shape)
                
        #print(output)

        loss = loss_fn(output, features)
        
        #model learns by back prop
        loss.backward()
        
        #optimize weights
        optimizer.step()
        #if change_lr == True: #change learning rate only when there is a change in teh epoch and not for each mini batch
        if change_lr == True: #change learning rate only when there is a change in teh epoch and not for each mini batch
            lr_scheduler.step(loss)
        
        running_loss += loss.item()
        
        #_, predicted = output.max(1)
        
        #print(output.shape)
        #print(output.max(1).shape)

        #mae_epoch+=torch.mean(torch.abs(output.max(1).values- features))
        lr = optimizer.param_groups[0]['lr']
        counter+=1

    training_loss = running_loss/len(trainloader)
    
    #ae = mae_epoch/counter
    learning_rate.append(lr)
    #training_mae.append(mae.cpu().detach().numpy())
    training_losses.append(training_loss)
    print('epoch: %d | Training Loss: %.3f  | LR: %.7f'%(epoch, training_loss, lr))
    
 # evaluation_accuracy = []
evaluation_losses = []
evaluation_mae = []
true_features = []
pred_features = []

def validation(epoch, testloader):
    model.eval() #similar to model.train(), model.eval() tells that you are testing. 
    running_loss = 0
    # total = 0
    # correct = 0
    counter = 0
    mae_epoch = 0
    

    for features, labels in testloader:
        #print(features[0].shape)
        if torch.cuda.is_available():
            features = features.cuda()
           
        features = features.to(dtype = torch.double)
       
        output = model(features) #predictions
        

        
        loss = loss_fn(output, features)


        
        running_loss += loss.item()

        #_, predicted = output.max(1)

        #mae_epoch+=torch.mean(torch.abs(output.max(1).values- labels))
        counter+=1
        
        #true_features.append(features)
        #pred_features.append(output.max(1).values)
        
    #else:
    #    print("Epoch {} - Testing loss: {}".format(i+1, running_loss/len(trainloader)))
    
    
    testing_loss = running_loss/len(testloader)
    # accuracy = 100.*correct/total
    #mae = mae_epoch/counter
    #return(accuracy) 
    # evaluation_accuracy.append(accuracy)
    evaluation_losses.append(testing_loss)
    #evaluation_mae.append(mae.cpu().detach().numpy())
    print('epoch: %d | Testing Loss: %.3f '%(epoch, testing_loss))
#tensorboard
# writer = S

layer_width_stack = np.array([ [1846,1846,1846,1846,1846,1846,1846,1846,1846,1846]])
                              
"""[2000,1800,1600,1300,1100,900,800,600,500,50],
                              [2000,1800,1600,1300,1100,900,800,600,500,100],
                              [2000,1800,1600,1300,1100,900,800,600,500,150],
                              [2000,1800,1600,1300,1100,900,800,600,500,200], 
                              [2000,1800,1600,1300,1100,900,800,600,500,250]])

                            [2000,1800,1600,1300,1100,900,800,600,500,300],
                              [2000,1800,1600,1300,1100,900,800,600,500,350],
                              [2000,1800,1600,1300,1100,900,800,600,500,350],
                              [2000,1800,1600,1300,1100,900,800,600,500,400],
                              [2000,1800,1600,1300,1100,900,800,600,500,450],
                              [2000,1800,1600,1300,1100,900,800,600,500,500],
                              [1846,1846,1846,1846,1846,1846,1846,1846,1846,1846]])
"""
for count, layer_width in enumerate(layer_width_stack):
    # Model Initialization
    model = AE(layer_width)
    model = model.double()
     
    # Validation using MSE Loss function
    loss_fn = torch.nn.MSELoss()
    
    if torch.cuda.is_available():
        model = model.cuda()
        loss_fn = loss_fn.cuda()
    
    # Using an Adam Optimizer with lr = 0.1
    optimizer = torch.optim.Adam(model.parameters(),
                                 lr = 1e-1,
                                 weight_decay = 1e-8)
    
    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.8, 
                                                              patience=50, threshold=0.0001, threshold_mode='rel', 
                                                              cooldown=0, min_lr=0.00001, eps=1e-08, verbose=False)
     
    
    trainloader_path = Path("../../../../data/processed/trainloaders/")
    testloader_path = Path("../../../../data/processed/testloaders/")
    trainloader_files = []
    testloader_files = []
    
    for (dirpath, dirnames, filenames) in walk(trainloader_path):
        trainloader_files.extend(filenames)
        break
    for (dirpath, dirnames, filenames) in walk(testloader_path):
        testloader_files.extend(filenames)
        break
    
    for epoch in range(20):
        change_LR = True
        for file in trainloader_files:
            trainloader = torch.load(Path(trainloader_path / file))
            train(epoch, trainloader, change_LR)
            change_LR = False
        for file in testloader_files:
            testloader = torch.load(Path(testloader_path / file))
            validation(epoch, testloader)

                
    feature_path = Path(Path() / "../../../../data/processed/trainingdata/SedFeatures_noNAN")
    features = np.load(Path(feature_path / "numpy_features.npy"))
    labels = np.load(Path(feature_path / "numpy_labebels.npy"))
    X_mean = np.load(Path(feature_path / "features_mean.npy")).astype(float)
    X_std = np.load(Path(feature_path / "features_std.npy")).astype(float)
    #lat_labels = np.load(Path(feature_path / "numpy_latitudes_labels.npy")).astype(np.float)
    #lon_labels = np.load(Path(feature_path / "numpy_longitudes_labels.npy")).astype(np.float)
    
    
    #Norm Features
    features = np.divide((features - X_mean),X_std)
    
    with open(Path(feature_path / "sorted_features.txt"), "rb") as fp:
        feature_names = pickle.load(fp)
    
    #feature_selection = pd.read_csv(Path("Selection_374_Chlor.csv")).values
    # feature_selection = pd.read_csv(Path("restrepoHighResFeaturesRaw-50.txt")).values
    
    
    #filter and clean features/labels
    #Features from list
    # selection_filter = np.zeros(features.shape[1], dtype=bool)
    # for idx, feature_name in enumerate(feature_names):
    #     if feature_name in feature_selection: # drop '.nc' from feature names #drop .grd
    #         selection_filter[idx] = True
    
    #Exclude Feature
    #selection_filter = np.ones(features.shape[1])
    # for idx, feature_name in enumerate(feature_names):
    #     if  selection_filter[idx] = 0
    
    # selection_filter = selection_filter.astype(bool)
    
    prediction_limit = 5000
    
    # features = features[:,selection_filter]
    features = features[labels<prediction_limit,:] #take only TOC < 5% 
    # lat_labels = lat_labels[labels<prediction_limit]
    # lon_labels = lon_labels[labels<prediction_limit]
    labels = labels[labels<prediction_limit]
    # labels[labels>prediction_limit] = prediction_limit
    
    #Drop features with NaNs
    nan_rows = np.isnan(features).any(axis=0)
    np.save("FeatureNanRows",nan_rows)
    features = features[:,~nan_rows]
    print(nan_rows)
    
    features_orig = features
    labels_orig = labels
    #features = features[:,0:1500]#testing if the autoencoder works better for lower dimensional data
    # #OR fill in NaNs with random
    # nan_fill = np.random.rand(features.shape[0],features.shape[1])*2-1
    # features[np.isnan(features)] = nan_fill[np.isnan(features)]
    
    
    #!!!!!!!! Donot shuffle!!!!
    # suffle features/labels
    #shuffle_idx = np.random.mtrand.randint(low = 0, high = len(labels), size = len(labels))
    #features_orig = features[shuffle_idx,:]
    #labels_orig = labels[shuffle_idx]
    
    

    
    #features = encoded_features.numpy()
    #print(features_orig.shape)
    
    """
    things to save:
        1. encoded features(to check if the encoder finds out the difference)
        2. frozen encoder with the optimized weights
    """

    
    #training_losses_all.append(training_losses)  
    #evaluation_losses_all.append(evaluation_losses)  
    #learning_rate_all.append(learning_rate)
    encoded_features = model.encoder(torch.from_numpy(features_orig).cuda())
    encoded_features_numpy = encoded_features.cpu().detach().numpy()
    np.save("../../../../data/processed/encoded_features/encoded_features_"+str(layer_width[-1])+".npy",encoded_features_numpy)
    #encoded_features_numpy = model(torch.from_numpy(features_orig).cuda()).cpu().detach().numpy()
    with open('log.txt', 'a') as f:
        f.write('decoded features')
        f.write(str(model.decoder(encoded_features)))
        f.write("Norm of difference of encoded features and the original features")
        f.write(str(np.linalg.norm(model.decoder(encoded_features).cpu().detach().numpy() - features_orig)))
        f.write("original features")
        f.write(str(features_orig))
    #print(norm_enocdedMinusOrig.append(np.linalg.norm(encoded_features_numpy - features_orig)))
    #plot_orig_vs_recon(model, layer_width, 'After training the encoder-decoder')
    
    
    plt.figure(figsize=[40,10])
    plt.plot(features_orig[0,0:500])
    plt.plot(model.decoder(encoded_features).cpu().detach().numpy()[0,0:500])
    plt.legend(["Original", "Reconstructed"])
    
    plt.savefig("../../../../reports/figures/origVSrecon_features_"+str(layer_width[-1])+".jpg")
    
    
    # save encoder
    torch.save(model.encoder, "../../../../data/saved_models/model_encoder_"+str(layer_width[-1])+".pth")
    torch.save(model.encoder.state_dict(), "../../../../data/saved_models/model_encoder_state_dict_"+str(layer_width[-1])+".pth")
    #torch.save(model.decoder, "../../../../data/saved_models/model_encoder_"+str(layer_width[-1])+".pth")
    
    #load model, use model.eval() to load the dropout and batch norm    
    model_encoder = torch.load("../../../../data/saved_models/model_encoder_"+str(layer_width[-1])+".pth")
    model_encoder.eval()
    
    encoded_features = model_encoder(torch.from_numpy(features_orig).cuda())
    encoded_features_numpy = encoded_features.cpu().detach().numpy()
