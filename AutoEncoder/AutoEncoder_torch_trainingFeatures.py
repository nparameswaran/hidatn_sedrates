#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  1 09:15:03 2022

@author: paramnav

Train an autoencoder with features(just at the training data points), 
to find the best latent space.
"""


import torch
from os import path, makedirs
from pathlib import Path
import pickle
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
#from sklearn.model_selection import StratifiedKFold, KFold
#from sklearn.model_selection import train_test_split
from torch.utils.data import TensorDataset, DataLoader
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
from torchsummary import summary
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA



np.random.seed(42)
torch.manual_seed(42)
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
#print(physical_devices)


print(device)


feature_path = Path(Path() / "../../../data/processed/trainingdata/SedFeatures_noNAN")
features = np.load(Path(feature_path / "numpy_features.npy"))
labels = np.load(Path(feature_path / "numpy_labebels.npy"))
X_mean = np.load(Path(feature_path / "features_mean.npy")).astype(float)
X_std = np.load(Path(feature_path / "features_std.npy")).astype(float)
#lat_labels = np.load(Path(feature_path / "numpy_latitudes_labels.npy")).astype(np.float)
#lon_labels = np.load(Path(feature_path / "numpy_longitudes_labels.npy")).astype(np.float)


#Norm Features
features = np.divide((features - X_mean),X_std)

with open(Path(feature_path / "sorted_features.txt"), "rb") as fp:
    feature_names = pickle.load(fp)

#feature_selection = pd.read_csv(Path("Selection_374_Chlor.csv")).values
# feature_selection = pd.read_csv(Path("restrepoHighResFeaturesRaw-50.txt")).values


#filter and clean features/labels
#Features from list
# selection_filter = np.zeros(features.shape[1], dtype=bool)
# for idx, feature_name in enumerate(feature_names):
#     if feature_name in feature_selection: # drop '.nc' from feature names #drop .grd
#         selection_filter[idx] = True

#Exclude Feature
#selection_filter = np.ones(features.shape[1])
# for idx, feature_name in enumerate(feature_names):
#     if  selection_filter[idx] = 0

# selection_filter = selection_filter.astype(bool)

prediction_limit = 5000

# features = features[:,selection_filter]
features = features[labels<prediction_limit,:] #take only TOC < 5% 
# lat_labels = lat_labels[labels<prediction_limit]
# lon_labels = lon_labels[labels<prediction_limit]
labels = labels[labels<prediction_limit]
# labels[labels>prediction_limit] = prediction_limit

#Drop features with NaNs
nan_rows = np.isnan(features).any(axis=0)
np.save("FeatureNanRows",nan_rows)
features = features[:,~nan_rows]
print(nan_rows)


# #OR fill in NaNs with random
# nan_fill = np.random.rand(features.shape[0],features.shape[1])*2-1
# features[np.isnan(features)] = nan_fill[np.isnan(features)]

# suffle features/labels
shuffle_idx = np.random.mtrand.randint(low = 0, high = len(labels), size = len(labels))
features_orig = features[shuffle_idx,:]
labels_orig = labels[shuffle_idx]

#features = encoded_features.numpy()
print(features_orig.shape)

# Norm features
features = np.divide((features - X_mean),X_std)


features = torch.tensor(features_orig)
labels = torch.tensor(labels_orig)

batch_size = 100

dataset=TensorDataset(features, labels)
dataloader=DataLoader(dataset, batch_size=batch_size)



testsize=int(features.shape[0]/10)
trainsize=features.shape[0]-testsize

#train_indices, val_indices = train_test_split(list(range(len(dataset.tensors[0]))), test_size=0.2, stratify=dataset.tensors[0])


train_set, test_set = torch.utils.data.random_split(dataset, [trainsize, testsize], generator=torch.Generator().manual_seed(42))

trainloader= torch.utils.data.DataLoader(train_set, batch_size=batch_size, shuffle=True)
testloader= torch.utils.data.DataLoader(test_set, batch_size=batch_size, shuffle=True)

num_channels = features.shape[1]

#####################################################summary of training set
# dataiter = iter(trainloader)
# features, labels = dataiter.next()

# print(features.shape)
# print(labels.shape)
######################################################

#print features shape
print(features.shape)

#are there any nan values in the features
np.mean(np.isnan(features.numpy()).any(axis=0))

class AE(torch.nn.Module):
    def __init__(self,  layer_width):
        super().__init__()
         
        # Building an linear encoder with Linear
        # layer followed by Relu activation function
        # 784 ==> 9
        self.encoder = torch.nn.Sequential(
            torch.nn.Linear(features.shape[1], layer_width[0]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[0], layer_width[1]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[1], layer_width[2]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[2], layer_width[3]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[3], layer_width[4]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[4], layer_width[5]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[5], layer_width[6]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[6], layer_width[7]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[7], layer_width[8])
        )
         
        # Building an linear decoder with Linear
        # layer followed by Relu activation function
        # The Sigmoid activation function
        # outputs the value between 0 and 1
        # 9 ==> 784
        self.decoder = torch.nn.Sequential(
            torch.nn.Linear(layer_width[8], layer_width[7]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[7], layer_width[6]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[6], layer_width[5]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[5], layer_width[4]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[4], layer_width[3]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[3], layer_width[2]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[2], layer_width[1]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[1], layer_width[0]),
            torch.nn.ReLU(),
            torch.nn.Linear(layer_width[0], features.shape[1])
        )
 
    def forward(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded
    
    
training_accuracy = []
training_losses = []
training_mae = []
learning_rate =  []

def train(epoch):
    model.train()   #tells the model that you are training the model. layers such as dropout and batchnorm which might 
                    #have a different characteristic during training and testing know what is going on. 
    running_loss = 0
    # total = 0
    # correct = 0
    counter = 0
    mae_epoch = 0
    running_vall_loss = 0.0 
    # running_accuracy = 0.0 
    # total = 0 
    
    for features, labels in trainloader:
        print(counter)
#         if type(features) == list:
#             print(features[0])
        #features = features[0]
        if torch.cuda.is_available():
            features = features.cuda()
            #labels = labels.cuda()


        
        #print(features[0].shape)
        features = features.to(dtype = torch.double)
        #print(features.shape)
        #labels   = labels.to(dtype = torch.double) 
        #summary(model, (num_channels, 7,7))
        #Training pass
    
        features = features.detach()
        optimizer.zero_grad()
        
        output = model(features) #predictions
        #labels = torch.reshape(labels, (labels.shape[0],1))
        #output = torch.reshape(output, (output.shape[0],1))
        
        #print(output.shape)
        #print(labels.shape)
                
        #print(output)

        loss = loss_fn(output, features)
        
        #model learns by back prop
        loss.backward()
        
        #optimize weights
        optimizer.step()
        #if change_lr == True: #change learning rate only when there is a change in teh epoch and not for each mini batch
        lr_scheduler.step(loss)
        
        running_loss += loss.item()
        
        _, predicted = output.max(1)
        
        #print(output.shape)
        #print(output.max(1).shape)

        #mae_epoch+=torch.mean(torch.abs(output.max(1).values- features))
        lr = optimizer.param_groups[0]['lr']
        counter+=1

    training_loss = running_loss/len(trainloader)
    
    #ae = mae_epoch/counter
    learning_rate.append(lr)
    #training_mae.append(mae.cpu().detach().numpy())
    training_losses.append(training_loss)
    if epoch%1 ==0:
        print('epoch: %d | Training Loss: %.3f  | LR: %.7f'%(epoch, training_loss, lr))
        
     # evaluation_accuracy = []
evaluation_losses = []
evaluation_mae = []
true_features = []
pred_features = []

def validation(epoch):
    model.eval() #similar to model.train(), model.eval() tells that you are testing. 
    running_loss = 0
    # total = 0
    # correct = 0
    counter = 0
    mae_epoch = 0
    

    for features, labels in testloader:
        #features = features[0]
        if torch.cuda.is_available():
            #features = features[0]
            features = features.cuda()
           
        features = features.to(dtype = torch.double)
       
        output = model(features) #predictions
        

        
        loss = loss_fn(output, features)


        
        running_loss += loss.item()

        _, predicted = output.max(1)

        #mae_epoch+=torch.mean(torch.abs(output.max(1).values- labels))
        counter+=1
        
        #true_features.append(features)
        #pred_features.append(output.max(1).values)
        
    #else:
    #    print("Epoch {} - Testing loss: {}".format(i+1, running_loss/len(trainloader)))
    
    
    testing_loss = running_loss/len(testloader)
    # accuracy = 100.*correct/total
    #mae = mae_epoch/counter
    #return(accuracy) 
    # evaluation_accuracy.append(accuracy)
    evaluation_losses.append(testing_loss)
    #evaluation_mae.append(mae.cpu().detach().numpy())
    if epoch%1 ==0:
        print('epoch: %d | Testing Loss: %.3f '%(epoch, testing_loss))
#tensorboard
# writer = S

layer_width_stack = np.array([[1500,1200,1000,800,600,400,200,100,50]])


def initialize_weights(m):
  # if isinstance(m, nn.BatchNorm1d):
  #     nn.init.constant_(m.weight.data, 1)
  #     nn.init.constant_(m.bias.data, 0)
  if isinstance(m, nn.Linear):
      nn.init.kaiming_normal_(m.weight.data)
      nn.init.constant_(m.bias.data, 0)

for layer_width in layer_width_stack:
    print(layer_width)
    
training_losses_all = []
evaluation_losses_all = []
learning_rate_all = []
norm_enocdedMinusOrig = []
for i, layer_width in enumerate(layer_width_stack):
    print("layer widths:")
    print(layer_width)

    # Model Initialization
    model = AE(layer_width)
    model.apply(initialize_weights)
    model = model.double()

    # Validation using MSE Loss function
    loss_fn = torch.nn.MSELoss()#MSELoss / L1Loss

    if torch.cuda.is_available():
        model = model.cuda()
        loss_fn = loss_fn.cuda()

    # Using an Adam Optimizer with lr = 0.1
    optimizer = torch.optim.Adam(model.parameters(),
                                 lr = 1e-1,
                                 weight_decay = 1e-8)

    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.8, 
                                                              patience=50, threshold=0.0001, threshold_mode='rel', 
                                                              cooldown=0, min_lr=0.00001, eps=1e-08, verbose=False)
    training_accuracy = []
    training_losses = []
    training_mae = []
    learning_rate =  []
    
    evaluation_losses = []
    evaluation_mae = []
    true_features = []
    pred_features = []
    for epoch in range(5):
        train(epoch)
        validation(epoch)
    
    training_losses_all.append(training_losses)  
    evaluation_losses_all.append(evaluation_losses)  
    learning_rate_all.append(learning_rate)
    encoded_features = model.encoder(torch.from_numpy(features_orig).cuda())
    encoded_features_numpy = encoded_features.cpu().detach().numpy()
    np.save("../../../data/processed/encoded_features/encoded_features_"+str(layer_width[-1])+".npy",encoded_features_numpy)
    encoded_features_numpy = model(torch.from_numpy(features_orig).cuda()).cpu().detach().numpy()

    norm_enocdedMinusOrig.append(np.linalg.norm(encoded_features_numpy - features_orig))
    