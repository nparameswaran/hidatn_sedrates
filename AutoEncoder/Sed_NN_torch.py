#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  7 12:22:06 2023

@author: sunms498
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 14:34:28 2022

@author: Ever & Naveen
"""

"""
TODO:

1. Create a class for the Neural network model
2. Be a child class of the pre-processing class(still needs to be done)
3. 
"""

# Libraries: torch, pandas, Path, numpy, pandas, scikit-learn, torchsummary
import torch
from os import path, makedirs
from pathlib import Path
import pickle
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
#from sklearn.model_selection import StratifiedKFold, KFold
#from sklearn.model_selection import train_test_split
from torch.utils.data import TensorDataset, DataLoader, Subset
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
from sklearn.metrics import mean_squared_error, mean_absolute_error
from torchsummary import summary
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
import time
import datetime
from torch_model import MLP
from coupled_model import coupled_model


np.random.seed(42)
torch.manual_seed(42)



##############################################################################   

    


class MLP_SedRate():
    def __init__(self):
        self.feature_path = Path(Path() / "/gxfs_work1/cau/sunms498/work/toc-neural-nets/SelectFeaturesRun/SedFeaturesnoNAN_nolimit/") #where is sedfeatures_nonan_nolimit?
        self.loadfeatureslabels()
        self.prediction_limit = 5000
        # selecting only some features(based on domain expertise)
        self.select_features = 0
        self.feature_selection = pd.read_csv("/gxfs_work1/cau/sunms498/work/toc-neural-nets/SelectFeaturesRun/Selection_374_Chlor.csv").values 
        
        # 0: the nan values are interpolated and hence features do not contain nans
        self.contains_nan = 0 if np.isnan(self.features).any() == 0 else 1
        
        # model parameters
        self.num_layers = 6
        self.activation = "relu"
        self.add_dropout = 0
        self.add_batchnorm = 1
        self.keras_initializer = "he_normal"
        
        self.min_lr = 1e-6
        self.do_rate = 0.2
        self.num_nodes = 50 # the number of nodes is constant across layers
        
        self.test_size = 0.2
        self.validation_split = 0.2
        self.numepochs = 1000
        self.batch_size = 70
        
        self.loss_fn = torch.nn.L1Loss()
        self.metrics = "RootMeanSquaredError" # for the validation
        self.learning_rate = 0.01 # starting value of the learning rate
        self.verbose = 1
        
        
        # self.model = MLP(self.features)
        self.model = coupled_model()
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=self.learning_rate)
        
        
        self.MAE = []
        self.MSE = []
        self.RMSE = []
        self.corrcoef = []
        self.R2_score = None
    
        
        self.device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

        print(self.device)

        #self.PCA_n_components = 100
        #training outputs
        self.training_accuracy = []
        self.training_losses = []
        self.training_mae = []
        self.learning_rate_list =  []
        
        #testing outputs
        self.evaluation_losses = []
        self.evaluation_mae = []
        self.true_labels = []
        self.pred_labels = []
        

        
 
        
##############################################################################   
 
        

        
    
    def loadfeatureslabels(self):
        self.features = np.load(Path(self.feature_path / "numpy_features.npy"))
        self.labels = np.load(Path(self.feature_path / "numpy_labebels.npy"))
        self.X_mean = np.load(Path(self.feature_path / "features_mean.npy")).astype(np.float)
        self.X_std = np.load(Path(self.feature_path / "features_std.npy")).astype(np.float)
        
    def preprocessing(self):
        print("Loading and Preprocessing data")
        #1. Norm the features
        self.features = np.divide((self.features - self.X_mean),self.X_std)
        
        #2. Select features(not recommended)
        if self.select_features == 1:
            with open(Path(self.feature_path / "sorted_features.txt"), "rb") as fp:
                feature_names = pickle.load(fp)


            selection_filter = np.zeros(self.features.shape[1], dtype=bool)
            for idx, feature_name in enumerate(feature_names):
                if feature_name[:-3] in self.feature_selection: # drop '.nc' from feature names
                    selection_filter[idx] = True
            self.features = self.features[:,selection_filter]
        
        #3. Set a prediction limit(trims high values)  
        self.features = self.features[self.labels<self.prediction_limit,:] #take only SedRate < Prediction Limit 
        self.labels = self.labels[self.labels<self.prediction_limit]
        
        #4. Remove nan values from features, if they contain
        if self.contains_nan:
            nan_rows = np.isnan(self.features).any(axis=0) #redundant
            self.features = self.features[:,~nan_rows]
        
        #5. Shuffle labels and features (does this help?)
        shuffle_idx = np.random.mtrand.randint(low = 0, high = len(self.labels), size = len(self.labels))
        self.features = self.features[shuffle_idx,:]
        self.labels = self.labels[shuffle_idx]
        
        #6. Convert to torch tensors
        
        self.features = torch.tensor(self.features)
        self.labels = torch.tensor(self.labels)
        self.dataset = TensorDataset(self.features, self.labels)
        
        
        
##############################################################################   
     
        
    def create_model_regression(self):
        print("Creating model")
        self.model.apply(self.initialize_weights)

 ##############################################################################   
            
        # initilize weights from a normal distribution: by default, pytorch initializes weights using lecun variance
    # https://stackoverflow.com/questions/48641192/xavier-and-he-normal-initialization-difference states that for relu: he initialization works better(refer paper)
    # https://adityassrana.github.io/blog/theory/2020/08/26/Weight-Init.html
    # it is a common practise to initialize the weights with kaiming he initialization and biases to zero, according to the 2014 Imagenet winner 

    def initialize_weights(m, self):
      if isinstance(m, nn.Linear):
          nn.init.kaiming_normal_(m.weight.data)
          nn.init.constant_(m.bias.data, 0)

                 ##############################################################################   
        
    def make_traintest_data(self):
        print("Making train and test data")
        # Create train data and test data
        
        testsize=int(self.features.shape[0]/10)
        trainsize=self.features.shape[0]-testsize
        
        
        train_set, test_set = torch.utils.data.random_split(self.dataset, [trainsize, testsize], generator=torch.Generator().manual_seed(42))
        
        
        # Create dataLoaders for training and testing data
        self.trainloader= torch.utils.data.DataLoader(train_set, batch_size=self.batch_size, shuffle=True)
        self.testloader= torch.utils.data.DataLoader(test_set, batch_size=self.batch_size, shuffle=True)
        
        # Print Size of batches
        
        print("Size of the training data batches")
        
        dataiter = iter(self.trainloader)
        features, labels = dataiter.next()

        print(features.shape)
        print(labels.shape)
        
##############################################################################   

    def run_model(self):
        print("Running model")

        #early stopping and LR-scheduling callbacks
        # early_stopping_cb = keras.callbacks.EarlyStopping(monitor = "loss", patience = 200, restore_best_weights = True)
        lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(self.optimizer, mode='min', factor=0.8, patience=50, threshold=0.0001, 
                           threshold_mode='rel', cooldown=0, 
                      min_lr=0.00001, eps=1e-08, verbose=False)
        
        
        self.model = self.model.to(self.device)
        self.loss_fn = self.loss_fn.to(self.device)
        start_time = time.time()

        #nested functions ahead!!
        def train(epoch):
            self.model.train()   #tells the model that you are training the model. layers such as dropout and batchnorm which might 
                            #have a different characteristic during training and testing know what is going on. 
            running_loss = 0
            # total = 0
            # correct = 0
            counter = 0
            mae_epoch = 0
            running_vall_loss = 0.0 
            # running_accuracy = 0.0 
            # total = 0 

            for features, labels in self.trainloader:
                features = features.to(self.device)
                labels = labels.to(self.device)

                features = features.to(dtype = torch.double)
                #print(features.shape)
                labels   = labels.to(dtype = torch.double) 
                #summary(model, (num_channels, 7,7))
                #Training pass
                self.optimizer.zero_grad()

                output = self.model(features) #predictions
                labels = torch.reshape(labels, (labels.shape[0],1))
                output = torch.reshape(output, (output.shape[0],1))

                #print(output.shape)
                #print(labels.shape)

                loss = self.loss_fn(output, labels)

                #model learns by back prop
                loss.backward()

                #optimize weights
                self.optimizer.step()
                lr_scheduler.step(loss)

                running_loss += loss.item()

                _, predicted = output.max(1)

                #print(output.shape)
                #print(output.max(1).shape)

                mae_epoch+=torch.mean(torch.abs(output.max(1).values- labels))
                lr = self.optimizer.param_groups[0]['lr']
                counter+=1

            training_loss = running_loss/len(self.trainloader)

            mae = mae_epoch/counter
            self.learning_rate_list.append(lr)
            self.training_mae.append(mae.cpu().detach().numpy())
            self.training_losses.append(training_loss)

            print('epoch: %d | Training Loss: %.3f | MAE: %.3f | LR: %.7f'%(epoch, training_loss,   mae, lr))
            
        def test(epoch, lastepoch = 0):
            self.model.eval() #similar to model.train(), model.eval() tells that you are testing. 
            running_loss = 0
            # total = 0
            # correct = 0
            counter = 0
            mae_epoch = 0
            
        
            for self.features, self.labels in self.testloader:
                if torch.cuda.is_available():
                    features = self.features.cuda()
                    labels = self.labels.cuda()
                
                features = features.to(dtype = torch.double)
                labels   = labels.to(dtype = torch.double)   
                #Training pass
                # optimizer.zero_grad()
                
                output = self.model(features) #predictions
                labels = torch.reshape(labels, (labels.shape[0],1))
                output = torch.reshape(output, (output.shape[0],1))
                
        
                
                loss = self.loss_fn(output, labels)
        
        
                
                running_loss += loss.item()
        
                _, predicted = output.max(1)
        
                mae_epoch+=torch.mean(torch.abs(output.max(1).values- labels))
                counter+=1
                if lastepoch == 1:
                    self.true_labels.append(labels)
                    self.pred_labels.append(output.max(1).values)
                
            #else:
            #    print("Epoch {} - Testing loss: {}".format(i+1, running_loss/len(trainloader)))
            
            
            testing_loss = running_loss/len(self.testloader)
            # accuracy = 100.*correct/total
            mae = mae_epoch/counter
            #return(accuracy) 
            # evaluation_accuracy.append(accuracy)
            self.evaluation_losses.append(testing_loss)
            self.evaluation_mae.append(mae.cpu().detach().numpy())
            print('epoch: %d | Testing Loss: %.3f | MAE: %.3f'%(epoch, testing_loss, mae))
        #tensorboard
        # writer = SummaryWriter()
        
        print("Training begins")
        for epoch in range(self.numepochs):
            train(epoch)
            if epoch >= (self.numepochs - 1):
                test(epoch, 1)
            else:
                test(epoch)
        
        #self.print_model_structure()
 
        print("Time taken: "+ str(time.time() - start_time))
        
##############################################################################   
    def print_model_structure(self):
        print(f"Model structure: {self.model}\n\n")

        for name, param in self.model.named_parameters():
            print(f"Layer: {name} | Size: {param.size()} | Values : {param[:2]} \n")
            
            
    

##############################################################################   
    def crossvalidation(self):
        # Split the data into k folds
        k = 5
        skf = KFold(n_splits=k, shuffle=True, random_state=42)
        
        # Initialize a list to store the evaluation metrics
        eval_metrics = []
        
        i = 1
        
        # Loop over the k folds
        for train_indices, val_indices in skf.split(self.features, self.labels):
            #reset class
            self.reset()
            
            # initilize random weights
            self.create_model_regression()
            
            # Create the Subset of the TensorDataset for the training set
            train_set = Subset(self.dataset, train_indices)
        
            # Create the Subset of the TensorDataset for the validation set
            val_set = Subset(self.dataset, val_indices)
        
            # Make dataloaders
            self.trainloader= torch.utils.data.DataLoader(train_set, batch_size=self.batch_size, shuffle=True)
            self.testloader= torch.utils.data.DataLoader(val_set, batch_size=self.batch_size, shuffle=True)
            
            # Train your model on the training set
            self.run_model()
            
            
            self.plot_model_performance(i)
            
            self.save_model(i)
            
            
                
            
            
            
            # Evaluate the model on the validation set and store the evaluation metrics
            #metrics = self.model.evaluate(val_set)
            #eval_metrics.append(metrics)
            i = i+1

##############################################################################   
    def reset(self):
        self.__init__()        
        
 

##############################################################################   

    def plot_model_performance(self, k = 0):
        plt.figure(figsize=[24,5])
        plt.subplot(131)
        plt.plot(self.training_mae)
        plt.plot(self.evaluation_mae)
        
        #plt.ylim([75,140])
        
        plt.subplot(132)
        plt.plot(self.training_losses)
        plt.plot(self.evaluation_losses)
        
        #plt.ylim([9000,25000])
        plt.legend(["training loss", "validation loss"])
        plt.subplot(133)
        plt.plot(self.learning_rate_list)
        plt.legend(["learning rate"])
        if k == 0:
            plt.savefig("../../../../reports/figures/ModelPerformancePlot_coupled.png")
        if k != 0:
            plt.savefig("../../../../reports/figures/ModelPerformancePlot_coupled" + str(k) + ".png")
        
        # save model
        X1_lastepoch = np.array([])
        Y1_lastepoch = np.array([])
        testsize=int(self.features.shape[0]/10)
        test_batches_per_epoch = int(testsize/self.batch_size) + 1 # if it is not entirly dividable, it just takes hte rest

        print(self.true_labels[-1].shape)
        print(self.pred_labels[-1].shape)
        
        #correlation plot
        plt.figure(figsize=[5,5])
        for i in range(test_batches_per_epoch):
            x1 = self.true_labels[-i].cpu().detach().numpy()
            x1 = x1.reshape([x1.shape[0],])
            y1 = self.pred_labels[-i].cpu().detach().numpy()
            line45deg = np.linspace(0,max(max(x1), max(y1)))
            plt.plot(x1, y1, 'b.')
            plt.plot(line45deg, line45deg, 'r-')
            X1_lastepoch = np.append(X1_lastepoch, x1)
            Y1_lastepoch = np.append(Y1_lastepoch, y1)
        #plt.subplot(134)
        
        print(X1_lastepoch.shape)
        print(Y1_lastepoch.shape)
        
        plt.title("Correlation Plot")
        plt.xlabel("True labels")
        plt.ylabel("Predicted labels")
        with open('log.txt', 'a') as f:
            f.write('correlation coeff')
            f.write(str(np.corrcoef(X1_lastepoch,Y1_lastepoch)[0,1]))
            f.write("\n")
            f.write('r2 score')
            f.write(str(r2_score(X1_lastepoch,Y1_lastepoch)))
            f.write("\n")

        

        
        if k == 0:
            plt.savefig("../../../../reports/figures/OriginalvsPredictedPlot_test_coupled.png")
        if k != 0:
            plt.savefig("../../../../reports/figures/OriginalvsPredictedPlot_test_coupled" + str(k) + ".png")       
            #return np.corrcoef(X1_lastepoch,Y1_lastepoch)[0,1]

        #correlation for all the labels? because the rf paper has it
        
        self.loadfeatureslabels()
        self.preprocessing()
        pred_labels_all = self.model(self.features.to(self.device)).cpu().detach().numpy()
        pred_labels_all = pred_labels_all.reshape([pred_labels_all.shape[0],])
        orig_labels_all = self.labels.cpu().detach().numpy()
        plt.figure(figsize=[5,5])
        plt.plot(orig_labels_all, pred_labels_all, 'b.')
        line45deg = np.linspace(0,max(max(orig_labels_all), max(pred_labels_all)))
        plt.plot(line45deg, line45deg, 'r-')
        plt.title("Correlation Plot for all labels")
        plt.xlabel("True labels")
        plt.ylabel("Predicted labels")
        with open('log.txt', 'a') as f:
            f.write('correlation coeff for all labels')
            f.write(str(np.corrcoef(orig_labels_all, pred_labels_all)[0,1]))
            f.write("\n")
            f.write('r2 score for all labels')
            f.write(str(r2_score(orig_labels_all, pred_labels_all)))
            f.write("\n")

        if k == 0:
            plt.savefig("../../../../reports/figures/OriginalvsPredictedPlot_all_coupled.png")
        if k != 0:
            plt.savefig("../../../../reports/figures/OriginalvsPredictedPlot_all_coupled" + str(k) + ".png")       
            #return np.corrcoef(X1_lastepoch,Y1_lastepoch)[0,1]


##############################################################################

    def calculate_plot_losses(self):
        print("Calculating correlation coefficients and errors")


        probs_stack = self.model(self.features_test,training = True).numpy() 
        np.savetxt("labels_test_regression.txt", self.labels_test)
        self.predictions = probs_stack
        np.savetxt("predictions_regression.txt", self.predictions)
        self.predictions = self.predictions.flatten()
        self.corrcoef = np.corrcoef(self.predictions, self.labels_test)[0,1]
        self.MAE = mean_absolute_error(self.labels_test,self.predictions)
        self.MSE = mean_squared_error(self.labels_test,self.predictions)
        self.RMSE = np.sqrt(mean_squared_error(self.labels_test,self.predictions))

        predicted_labels = self.model(self.features,training = True)
        self.R2_score = r2_score(self.labels, predicted_labels)
        
        
        #why do we take the sum and average it?
        #if there are a  lot of models because of the number of mc samples, then we take the average. But normally the length is one and the sum is just one value. hence it does not make a diffference.
        print("Correlation Coefficient: "+ str(self.corrcoef))
        print("MAE: " +str(self.MAE))
        print("MSE: " +str(self.MSE))
        print("RMSE: " +str(self.RMSE))
        print("R2_score: "+ str(self.R2_score))
        
        plt.figure(figsize=[8,8])
        plt.scatter(self.predictions,self.labels_test)
        x = np.linspace(0,1.1*self.prediction_limit)
        plt.plot(x, x, 'r')
        plt.xlabel("Predicted (cm/ka)")
        plt.ylabel("Observed (cm/ka)")

##############################################################################   
    def PCA_features(self, n_components = 100):
        pca = PCA (n_components) #95% of the variance is retained
        pca.fit(self.features)
        self.features = pca.transform(self.features)
        print("Transforming features using PCA: new dimensions of features")
        #print("help")


##############################################################################       
    def save_model(self, i=0):
        torch.save(self.model, "../../../../data/saved_models/coupled_model_"+str(i)+".pth")
        # torch.save(self.model.supervised_part2, "../../../../data/saved_models/model_supervised_part2_"+str(i)+".pth")
        # torch.save(self.model, "../../../../data/saved_models/model_supervised_"+str(i)+".pth")
        


##############################################################################       
if __name__ == '__main__':
    print("step0: load features and labels")
    model1 = MLP_SedRate()
    print("step1: preprocessing")
    model1.preprocessing()
    model1.PCA_features()
    print("step2: create model")
    model1.create_model_regression()
    print("step3: crossvalidation")
    model1.crossvalidation()
    #print("step3: make train test dataset")
    #model1.make_traintest_data()
    #print("step4: run model")
    #model1.run_model()
    print("step5: plot model performance")
    model1.plot_model_performance()
    
    print("step6: save model")
    #print("step6: calculate and plot losses")
    #model1.calculate_plot_losses()



"""

after cross validation, which model do we take? do we save all the models? and take the best one?
"""
