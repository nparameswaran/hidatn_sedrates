#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb  7 17:21:55 2023

@author: sunms498
"""

# attach both models
# step 1: load the models
# step 2: use supervised training again
# step 3: get results

import torch
import torch.nn as nn

model_encoder = torch.load("../../../../data/saved_models/model_encoder_400.pth")
model_supervised_part2 = torch.load("../../../../data/saved_models/model_supervised_part2_2.pth")

torch.set_default_dtype(torch.double)


class coupled_model(nn.Module):
    def __init__(self):
        super(coupled_model, self).__init__()
        self.modelA = model_encoder
        self.modelB = model_supervised_part2
        
    def forward(self, x):
        x = self.modelA(x)
        x = self.modelB(x)
        return x                                   
