#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  9 15:54:04 2023

@author: sunms498
"""

import torch
from pathlib import Path
import pickle
import numpy as np
from matplotlib import pyplot as plt
#from sklearn.model_selection import StratifiedKFold, KFold
#from sklearn.model_selection import train_test_split
import os
import gc


model = torch.load("../../../../data/saved_models/coupled_model_1.pth")

model.eval()


os.chdir("/gxfs_work1/cau/sunms498/work/david/NN4SedRatePred/models/MLP/SedRate_MAR/models_torch/")


np.random.seed(42)
torch.manual_seed(42)
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu') #'cuda:0' if torch.cuda.is_available() else 
#print(physical_devices)
model.to(device)
print(device)

from os import walk  
feature_path = Path(Path() / "../../../../data/processed/trainingdata/SedFeatures_noNAN")
X_mean = np.load(Path(feature_path / "features_mean.npy")).astype(np.float)
X_std = np.load(Path(feature_path / "features_std.npy")).astype(np.float)

#dataset_path = Path(Path().resolve().parents[2] / "Data" / "LeeKNNsTOC" / "WorldFeaturesAll")#ChlorWorldFeatures
dataset_path = Path("../../../../../../../Data/RestrepoKNNsSed/WorldFeaturesAllnoNaN")
files = []

for (dirpath, dirnames, filenames) in walk(dataset_path):
    files.extend(filenames)
    break

files.sort()


features = np.load(Path(dataset_path / files[0]))

chunk_shape = [6,4320] #360 chunks


first_run = True
ii = 0
for count, file in enumerate(files):
    



    features = np.load(Path(dataset_path / file))
    # features = features[:,selection_filter]
    #features = features[:,~nan_filter] #filter NaNs

    #Norm Features
    features = np.divide((features - X_mean),X_std)
    

    features = torch.tensor(features)
    
    features = features.to(device)
    
    predictions = model(features)
    predictions = predictions.cpu().detach().numpy()
    predictions = predictions.reshape(chunk_shape,  order = 'C')
    
    if first_run:
        prediction_map = predictions
        first_run = False
    else:
        prediction_map = np.append(prediction_map, predictions, axis = 0)
        
    ii+=1
    print("prediction " + str(ii) + " done!")
    
    del(features)
    gc.collect()
    
    
# land_file = Path(Path().resolve().parents[2] / "Data" / "LeeKNNsTOC" / "island_map.npy")
# land_map = np.load(land_file)
# land_map[np.isnan(land_map)] = 1
#prediction_map[land_map] = np.nan
#plt.imshow(prediction_map)
# np.save("Sed_regresion_deep_RestrepoFeats_AllVS0.2_nolimit", prediction_map)
#prediction_map = np.load("MAR_prediction_map(log(x+1))RestrepoFeats__AllVS0.2.npy")
# from matplotlib.colors import LogNorm
#plot_TOC = 3
# plot_sed = 100
# 

prediction_map = np.rot90(np.rot90(np.fliplr(prediction_map)))
land_file = Path("/gxfs_work1/cau/sunms498/Data/is_land_map.npy")
land_map = np.load(land_file)
land_map[np.isnan(land_map)] = 1
prediction_map[land_map] = 0

plt.figure(figsize=[25,15], dpi=200)

plt.imshow(prediction_map,vmin =0, vmax=500, cmap = "cubehelix")
cbar = plt.colorbar()
